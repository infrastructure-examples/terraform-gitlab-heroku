terraform {
  backend "pg" {
    conn_str = "STATE_DB_URL"
  }
  required_version = "= 0.12.21"
}

provider "heroku" {
  api_key = "HEROKU_API_KEY"
  email   = "HEROKU_EMAIL"
  version = "~> 2.0"
}

resource "heroku_app" "production" {
  name   = "testing-terraform-production"
  region = "us"
}
